# install xcode 
xcode-select --install

# install brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update && brew upgrade && brew doctor

# install brew packages
brew install python jq git node postgresql zsh zsh-syntax-highlighting wget

# install brew-cask apps + fonts
brew tap homebrew/cask-fonts
brew cask install google-chrome flux iterm2 spotify postman spectacle vlc visual-studio-code wireshark imageoptim firefox onyx java kap
brew cask install font-jetbrains-mono

# install python things
python3 -m pip install --user --upgrade pip
python3 -m pip install --user pipenv jupyter ipdb

# add zsh syntax highlighting
cd ~/ && git clone https://github.com/zsh-users/zsh-syntax-highlighting.git .zsh-syntax-highlighting

# change to zsh
chsh -s /bin/zsh

# install terminal theme
npm install --global pure-prompt

# change iterm preferences
mkdir ~/iterm-preferences
cp src/com.googlecode.iterm2.plist ~/iterm-preferences

# use .zshrc file
cp src/.zshrc ~/
source ~/.zshrc

# install vscode extensions
code --install-extension eamodio.gitlens
code --install-extension zhuangtongfa.material-theme
code --install-extension ms-python.python
code --install-extension octref.vetur
code --install-extension esbenp.prettier-vscode
code --install-extension dbaeumer.vscode-eslint
code --install-extension firsttris.vscode-jest-runner
code --install-extension redhat.vscode-yaml
code --install-extension rebornix.ruby
code --install-extension editorconfig.editorconfig
code --install-extension vayan.haml

# update osx
sudo softwareupdate -i -a -R
