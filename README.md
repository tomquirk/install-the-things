# ireset

## Base install

1. Run install script

```bash
chmod +x install.sh && .install.sh
```

1. [Docker for mac](https://www.docker.com/docker-mac)
1. [Google Drive](https://www.google.com/drive/download/)
1. [postgres.app](https://postgresapp.com/)

## Settings

### Mac settings

- trackpad - _not_ natural and tap to click
- key repeat - fast
- delay - 4
- dock left
- Accessibility - turn transparency and motion off

### cli

1. Set Git settings (**NOTE: change email if work machine**)

```
git config --global user.name "Tom Quirk"
git config --global user.email "tomquirkacc@gmail.com"
```

1. Generate ssh keys (**NOTE: change email if work machine**)

```
ssh-keygen -t rsa -b 4096 -C "tomquirkacc@gmail.com"
```

1. Copy ssh keys and `chmod 700 ~/.ssh/id_rsa`
1. update pyhon bin location in .zshrc to match installed pyton version

## App specific

### iterm

1. Update custom preference folder in iterm (Preferences -> General -> Preferences tab). Set it to `~/iterm-preferences`
2. Import `src/tomquirk-iterm.json` iTerm profile (Preferences -> Profiles -> Other Actions -> Import JSON profiles)
