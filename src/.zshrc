export SCRIPTS=~/.scripts
export PATH="$SCRIPTS:$PATH"

# brew
export PATH="/usr/local/sbin:$PATH"
alias brewup="brew update && brew upgrade && brew doctor && brew cleanup"

# zsh
source ~/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
autoload -U promptinit; promptinit
prompt pure

export HISTSIZE=5000
export HISTFILE=~/.zsh_history
export SAVEHIST=$HISTSIZE

# Better CLI
# alias cat='bat'
alias top="sudo htop" # alias top and fix high sierra bug
alias ls="ls -G"

# python
export PATH="/Users/tomquirk/Library/Python/3.6/bin:$PATH"

# java
export JAVA_HOME=$(/usr/libexec/java_home)

# vscode
export PATH="\$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"

